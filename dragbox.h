#ifndef DRAGBOX_H
#define DRAGBOX_H

#include <QWidget>
#include <QStringList>
#include <QHBoxLayout>
#include <QPair>

class DragBox : public QWidget
{
    Q_OBJECT
public:
    DragBox(QStringList words);
    virtual ~DragBox();
    bool getResults(QStringList& orig, QStringList& fixed);
signals:
    void newCompletionStatus(bool);
protected:
    void mousePressEvent(QMouseEvent *event);
    void dropEvent(QDropEvent *event);
    void dragEnterEvent(QDragEnterEvent* event);
private:
    QVector<QWidget*> topWidgets;
    QVector<QWidget*> bottomWidgets;
    QGridLayout* topRow;
    QGridLayout* bottomRow;
};

#endif // DRAGBOX_H
