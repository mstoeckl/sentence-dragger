#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QComboBox>
#include <QGridLayout>
#include <QPushButton>

class DragBox;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void readfile();
    void pickset(int i);
    void dumpData();
private:
    void loadDataSet(QVector<QStringList> x);

    DragBox* dragbox;
    QWidget* dbproxy;
    QVector<QStringList> dataset;
    QComboBox* chooser;
    QGridLayout* layout;
    QPushButton* dumpButton;
};

#endif // MAINWINDOW_H
