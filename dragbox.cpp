#include "dragbox.h"

#include <QLabel>
#include <QHBoxLayout>
#include <QDrag>
#include <QMouseEvent>
#include <QDragMoveEvent>
#include <QApplication>
#include <QMimeData>
#include <QFrame>


QWidget* makeLabel(QString word) {
    QLabel* l1 = new QLabel(word);
    l1->setMinimumWidth(150);
    QHBoxLayout* h1 = new QHBoxLayout();
    h1->addWidget(l1);
    QFrame* f1 = new QFrame();
    f1->setLayout(h1);
    f1->setObjectName(word);
    f1->setFrameShadow(QFrame::Raised);
    f1->setFrameShape(QFrame::StyledPanel);
    return f1;
}

QWidget* makeDummy() {
    return makeLabel("");
}

DragBox::DragBox(QStringList words) : QWidget()
{
    topRow = new QGridLayout();
    bottomRow = new QGridLayout();
    // 6xWhatever
    for (int i=0;i<words.size();i++) {
        const QString& s = words[i];
        QWidget* s1 = makeLabel(s);
        QWidget* s2 = makeLabel(s);
        topRow->addWidget(s1, i / 6, i % 6);
        bottomRow->addWidget(s2, i / 6, i % 6);
        topWidgets.append(s1);
        bottomWidgets.append(s2);
    }

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(topRow);
    layout->addSpacing(10);
    layout->addLayout(bottomRow);
    layout->addStretch(1);
    this->setLayout(layout);

    setAcceptDrops(true);
}

DragBox::~DragBox() {

}

bool DragBox::getResults(QStringList& orig, QStringList& fixed) {
    QStringList a;
    QStringList b;
    for (QWidget* w : bottomWidgets) {
        if (w->objectName().isEmpty()) {
            return false;
        }
        b << w->objectName();
    }
    for (QWidget* w : topWidgets) {
        a << w->objectName();
    }
    orig = a;
    fixed = b;
    return true;
}


void DragBox::mousePressEvent(QMouseEvent *event) {
    if (event->button() != Qt::LeftButton)
        return;
    QVector<QWidget*> allWidgets = bottomWidgets + topWidgets;// order matters
    for (int i=0;i<allWidgets.size();i++) {
        QWidget* label = allWidgets[i];
        if (label->geometry().contains(event->pos())) {
            QPoint hotSpot = event->pos() - label->pos();
            QMimeData *mimeData = new QMimeData;

            mimeData->setText(label->objectName());
            mimeData->setObjectName(QString::number(i));

            QPixmap pixmap(label->size());
            label->render(&pixmap);

            QDrag *drag = new QDrag(this);
            drag->setMimeData(mimeData);
            drag->setPixmap(pixmap);
            drag->setHotSpot(hotSpot);

            Qt::DropAction dropAction = drag->exec(Qt::CopyAction | Qt::MoveAction, Qt::CopyAction);
            if (dropAction == Qt::MoveAction)
                    label->close();
            return;
        }
    }
}

void DragBox::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasText()) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}

void DragBox::dropEvent(QDropEvent *event) {
    if (event->mimeData()->hasText()) {
        for (QWidget* label : topWidgets) {
            if (label->geometry().contains(event->pos())) {
                // If you land here, glow red for half a second
                event->accept();
                return;
            }
        }
        for (int i=0;i<bottomWidgets.size();i++) {
            QWidget* label = bottomWidgets[i];
            if (label->geometry().contains(event->pos())) {
                const QString& orig = label->objectName();
                const QString& next = event->mimeData()->text();

                int num_orig = 0;
                int num_next = 0;
                for (int j=0;j<topWidgets.size();j++) {
                    if (topWidgets[j]->objectName() == orig) num_orig++;
                    if (topWidgets[j]->objectName() == next) num_next++;
                }

                if (orig == next) {
                    // Duplicate, no action
                } else {
                    // Make change
                    QWidget* w = makeLabel(next);
                    label->close();
                    bottomRow->replaceWidget(label, w);
                    bottomWidgets[i] = w;

                    // Cleanup
                    int cur_orig = 0;
                    int cur_next = 0;
                    for (int j=0;j<bottomWidgets.size();j++) {
                        if (bottomWidgets[j]->objectName() == orig) cur_orig++;
                        if (bottomWidgets[j]->objectName() == next) cur_next++;
                    }
                    if (cur_next > num_next) {
                        // Nuke the spot pulled from
                        int oldloc = event->mimeData()->objectName().toInt();
                        if (oldloc < bottomWidgets.size()) {
                            QWidget* b = bottomWidgets[oldloc];
                            b->close();
                            QWidget* d = makeDummy();
                            bottomWidgets[oldloc] = d;
                            bottomRow->replaceWidget(b, d);
                        } else {
                            for (int k=bottomWidgets.size()-1;k>=0;k--) {
                                QWidget* b = bottomWidgets[k];
                                if (k != i && b->objectName() == next) {
                                    b->close();
                                    QWidget* d = makeDummy();
                                    bottomWidgets[k] = d;
                                    bottomRow->replaceWidget(b, d);
                                    break;
                                }
                            }
                        }
                        cur_next--;
                    }
                    // Update highlighting
                    for (int k=topWidgets.size() - 1, left=num_orig-cur_orig; k>=0;k--) {
                        if (topWidgets[k]->objectName() == orig) {
                            if (left > 0) {
                                topWidgets[k]->setStyleSheet("QFrame { background: yellow }");
                                left--;
                            } else {
                                topWidgets[k]->setStyleSheet("");
                            }
                        }
                    }
                    for (int k=topWidgets.size() - 1, left=num_next-cur_next; k>=0;k--) {
                        if (topWidgets[k]->objectName() == next) {
                            if (left > 0) {
                                topWidgets[k]->setStyleSheet("QFrame { background: yellow }");
                                left--;
                            } else {
                                topWidgets[k]->setStyleSheet("");
                            }
                        }
                    }
                }

                event->accept();

                for (QWidget* w : bottomWidgets) {
                    if (w->objectName().isEmpty()) {
                        emit newCompletionStatus(false);
                        return;
                    }
                }
                emit newCompletionStatus(true);
                return;
            }
        }
    } else {
        event->ignore();
    }
}
