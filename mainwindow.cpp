#include "mainwindow.h"
#include "dragbox.h"

#include <QMenu>
#include <QMenuBar>
#include <QFileDialog>
#include <QRegExp>
#include <QGridLayout>
#include <QLabel>
#include <QComboBox>



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QMenu* p = this->menuBar()->addMenu("Open");
    p->addAction("Open data", this, SLOT(readfile()), QKeySequence::Open);

    // TODO: use UI file instead?
    chooser = new QComboBox();
    connect(chooser, SIGNAL(currentIndexChanged(int)), this, SLOT(pickset(int)));
    dragbox = (DragBox*) new QWidget();

    dumpButton = new QPushButton("Dump");
    dumpButton->setStyleSheet("QPushButton { color : green }");
    dumpButton->setEnabled(false);
    connect(dumpButton, SIGNAL(clicked(bool)), this, SLOT(dumpData()));

    layout = new QGridLayout();
    layout->addWidget(chooser,0,0);
    layout->addWidget(dumpButton, 2, 0);
    layout->setColumnStretch(0,0);
    layout->addWidget(dragbox,0,2, 4, 1);
    layout->setRowMinimumHeight(1,10);
    layout->setColumnMinimumWidth(1,10);
    layout->setRowStretch(3, 1);
    layout->setColumnStretch(4, 1);

    QWidget* cwidget = new QWidget();
    cwidget->setLayout(layout);
    this->setCentralWidget(cwidget);

    QStringList ex;
    ex << "We" << "mean" << "no" << "harm" << "to" << "you.";
    loadDataSet(QVector<QStringList>(1, ex));
}

MainWindow::~MainWindow()
{

}

void MainWindow::pickset(int i) {
    DragBox* nextbox = new DragBox(dataset[i]);
    dragbox->close();
    layout->replaceWidget(dragbox, nextbox);
    dragbox->deleteLater();
    dragbox = nextbox;

    dumpButton->setEnabled(true);
    connect(dragbox, SIGNAL(newCompletionStatus(bool)), dumpButton, SLOT(setEnabled(bool)));
}

void MainWindow::loadDataSet(QVector<QStringList> x) {
    dataset = x;
    chooser->blockSignals(true);
    chooser->clear();
    for (int i=0;i<dataset.size();i++) {
        chooser->addItem(QString::number(i+1));
    }
    chooser->blockSignals(false);

    pickset(0);
}

void MainWindow::readfile() {
    QString fn = QFileDialog::getOpenFileName();
    if (fn.isEmpty()) {
        return;
    }
    QFile fo(fn);
    fo.open(QIODevice::ReadOnly);
    QByteArray bytes = fo.readAll();
    if (bytes.isNull()) {
        qWarning("File %s could not be read.", fn.toUtf8().constData());
        return;
    }
    QString text(bytes);
    QStringList lines = text.split("=====");
    QVector<QStringList> data;
    for (QString s : lines) {
        QStringList words = s.split(QRegExp("[ \n\t]+"),QString::SkipEmptyParts);
        data.append(words);
    }
    loadDataSet(data);
}

void MainWindow::dumpData() {
    dumpButton->setEnabled(false);
    QStringList orig;
    QStringList fixed;
    if (dragbox->getResults(orig, fixed)) {
        QFile f("log.dat");
        f.open(QIODevice::Append);
        f.write("=====\n");
        f.write(QString(orig.join(" ")).toUtf8());
        f.write("\n=====\n");
        f.write(QString(fixed.join(" ")).toUtf8());
        f.write("\n");
    } else {
        qWarning("Failed to get results.");
    }

}
