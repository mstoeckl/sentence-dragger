#-------------------------------------------------
#
# Project created by QtCreator 2016-01-28T13:21:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sentence_dragger
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dragbox.cpp

HEADERS  += mainwindow.h \
    dragbox.h

QMAKE_CXXFLAGS += -std=c++11
